import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic_react',
  webDir: 'build',
  bundledWebRuntime: false,
  // server : {
  //   url : "http://172.16.1.124:3000"
  // }
};

export default config;
