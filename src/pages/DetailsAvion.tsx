import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonRefresher,
    IonRefresherContent,
    IonTitle,
    IonToolbar,
    RefresherEventDetail,
    useIonViewWillEnter,
} from '@ionic/react';
import { useEffect, useState } from 'react';
import { Redirect, useParams } from 'react-router';
import Details from '../components/Details';
import Navbar from '../components/Navbar';
import { Avion } from '../data/interfaceResponse';
import './Home.css';
  
const DetailsAvion: React.FC = () => {
    const params = useParams<{id: string}>();
    const [avionDetails, setAvion] = useState<Avion>();

    if (localStorage.getItem("token") == null) {
      return <Redirect to="/login/1"/>;
    }else{
      // eslint-disable-next-line react-hooks/rules-of-hooks
      useIonViewWillEnter(()=>{
        const token = JSON.parse(localStorage.getItem("token")!);
        const url = "https://avion-production.up.railway.app/avions/"+params.id+"?idU="+token.idUser+"&token="+token.token;
        fetch(url)
        .then((response)=>response.json())
        .then((avions)=>{
          setAvion(avions.data);
          console.log(avions.data);
        })
      },[]);

    }


    function handleRefresh(event: CustomEvent<RefresherEventDetail>){
      setTimeout(() =>{
        const token = JSON.parse(localStorage.getItem("token")!);
        const url = "https://avion-production.up.railway.app/avions/"+params.id+"?idU="+token.idUser+"&token="+token.token;
        fetch(url)
        .then((response)=>response.json())
        .then((avions)=>{
          setAvion(avions.data);
          console.log(avions.data);
        })
      },2000 )
    }

    return (
      <IonPage id="home-page">
          <IonRefresher slot="fixed" onIonRefresh={handleRefresh}>
            <IonRefresherContent></IonRefresherContent>
          </IonRefresher>
          <Navbar contentId="main-content" />
          <IonContent id="main-content">
            <IonHeader>
              <IonToolbar>
                <IonButtons slot="start">
                  {/* Button that toggle the navbar */}
                  <IonMenuButton></IonMenuButton>
                </IonButtons>
                <IonTitle>Avion</IonTitle>
              </IonToolbar>
            </IonHeader>
              <Details avion={avionDetails} />
          </IonContent>
      </IonPage>
    );
};
  
export default DetailsAvion;
  