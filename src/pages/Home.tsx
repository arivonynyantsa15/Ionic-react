import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { useEffect, useState } from 'react';
import ListeVehicule from '../components/ListeAvion';
import Navbar from '../components/Navbar';
import './Home.css';

const Home: React.FC = () => {
  const [data, setData] = useState([])

  useEffect(()=>{
    fetch("https://avion-production.up.railway.app/avions")
    .then((response)=>response.json())
    .then((avions)=>{
      setData(avions.data);
    })
  },[]);

  return (
    <IonPage id="home-page">
        <Navbar contentId="main-content" />
        <IonContent id="main-content">
          <IonHeader>
            <IonToolbar>
              <IonButtons slot="start">
                {/* Button that toggle the navbar */}
                <IonMenuButton></IonMenuButton>
              </IonButtons>
              <IonTitle>Avion</IonTitle>
            </IonToolbar>
          </IonHeader>
          <ListeVehicule avions={data} />
        </IonContent>
    </IonPage>
  );
};

export default Home;
