import { InputChangeEventDetail,
     IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonList, IonPage, IonToolbar } from '@ionic/react';
import React from 'react';
import CardResponse from '../components/CardResponse';



export default class Login extends React.Component<{message: string}, {email : string, motDePasse:string, error:string}> {

    constructor(props: { message: string }){
        super(props);
        this.state = {
            email : '',
            motDePasse : '',
            error : ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    async sendDatas(){
        let url = 'https://avion-production.up.railway.app/login';
        const res = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        });
        const result_1 = res.json();
        return result_1;
    }

    handleInputChange(e: any){
        const target = e.target;
        const nom = target.name;

        this.setState({
            ...this.state,
            [nom] : target.value
        });
    }

    async handleSubmit(e: { preventDefault: () => void; }){
        e.preventDefault();
        const result = await this.sendDatas();
        console.log(result);
        if (result.data != null) {
            localStorage.setItem('token', JSON.stringify(result.data));
            this.setState({
                error : 'Vous etes maintenant connecte'
            });
        }else{
            this.setState({
                error : result.error.message
            })
        }
        console.log(this.state);
    }
    
    render(){
        let response;
        if(this.props.message !== ''){
            response =  <CardResponse message={this.props.message}/>;
        }
        if(this.state.error !== ''){
            response =  <CardResponse message={this.state.error}/>;
        }
        return (
            <IonPage id="view-message-page">
                <IonHeader translucent>
                    <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton text="Inbox" defaultHref="/home"></IonBackButton>
                    </IonButtons>
                    </IonToolbar>
                </IonHeader>

                <IonContent>

                    <IonList>
                        <h3> Login </h3>
                        <form onSubmit={this.handleSubmit}>
                            <IonItem>
                                <IonLabel position='floating'> Email : jean@gmail.com </IonLabel>
                                <IonInput type="text" onIonChange={this.handleInputChange} name="email" /> 
                            </IonItem>

                            <IonItem>
                                <IonLabel  position='floating'>Mot de passe : jean</IonLabel>
                                <IonInput type="password" onIonChange={this.handleInputChange} name="motDePasse"/>
                            </IonItem>
                        <IonButton className='ion-margin-top inputitem' type='button' expand='block' onClick={this.handleSubmit}>
                            Login
                        </IonButton>
                        </form>
                    </IonList>
                    {response}
                </IonContent>
            </IonPage>
        );
    }


}