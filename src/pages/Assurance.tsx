import { useState } from 'react';
import {
  IonBackButton,
  IonButtons,
  IonHeader,
  IonPage,
  IonToolbar,
  useIonViewWillEnter,
} from '@ionic/react';
import { useParams } from 'react-router';
import './ViewMessage.css';
import ListeAssurances from '../components/ListeAssurances';

function Assurance() {
  const [assurances, setAssurance] = useState([]);
  //num as the number max of the month
  const params = useParams<{ num: string }>();

  useIonViewWillEnter(() => {
    const num = parseInt(params.num, 10);
    fetch("https://avion-production.up.railway.app/avions/assurance-expire/" + num)
      .then((response) => response.json())
      .then((assurances) => {
        setAssurance(assurances.data);
      })
  });

  return (
    <IonPage id="view-message-page">
      <IonHeader translucent>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton text="Assurance" defaultHref="/home"></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <ListeAssurances assurances={assurances} />
    </IonPage>
  );
}

export default Assurance;
