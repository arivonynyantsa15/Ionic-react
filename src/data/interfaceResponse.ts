export interface Avion {
    id: number,
    marque: Marque,
    nImmatriculation : string 
    image : string 
    format : string
    trajets : Trajet[]
}

export interface Assurance {
    id : number,
    avion : Avion,
    dateFin : string,
    dateDebut : string
}

export interface Marque {
    id: number,
    nom: string
}

export interface Trajet {
    id: number,
    idVehicule: number,
    dateTrajet: string,
    debutKm: number,
    finKm: number 
}