import {
  IonContent,
  IonHeader,
  IonList,
  IonMenu,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import NavbarItem from "./NavbarItem";

export default function Navbar(props: { contentId: string | undefined; }) {
    return (
        // contentId target the contentIon to add a menu 
        <IonMenu contentId={props.contentId}>
            <IonHeader>
                <IonToolbar>
                <IonTitle>Menu Content</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent className="ion-padding">
                <IonList>
                    <NavbarItem url="/home" label="Home" />
                    <NavbarItem url="/login" label="Login" />
                    <NavbarItem url="/assurance-expire/3" label="Assurance expire dans 3 mois" />
                    <NavbarItem url="/assurance-expire/1" label="Assurance expire dans 1 mois" />
                </IonList>
            </IonContent>
        </IonMenu>
    );
}
