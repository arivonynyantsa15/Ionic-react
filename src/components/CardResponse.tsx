import { IonCard, IonCardContent } from "@ionic/react";
import React from "react";

export default function CardResponse(props:{message : string}){

    return (
        <IonCard>
            <IonCardContent>
                {props.message}
            </IonCardContent>
        </IonCard>

    )

}