import {
  IonButton,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonItem,
  IonList,
  IonRow,
  IonTitle,
  useIonViewWillEnter,
} from "@ionic/react";
import { useState } from "react";
import { Redirect } from "react-router";
import { Avion, Trajet } from "../data/interfaceResponse";
import { Camera, CameraResultType, Photo } from "@capacitor/camera";

import "./details.css";

export default function Details(props: { avion: Avion | undefined }) {
  const [image, setImage] = useState<Photo | undefined>();

  const updateImage = async (photo : Photo) => {
    let datas = {
      idAvion: props.avion?.id,
      image: photo.base64String,
      format: photo.format,
    };
    console.log(datas);
    let url = "https://avion-production.up.railway.app/avions/image";
    const res = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(datas),
    });
  };

  const uploadImage = async () => {
    let photo = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
    });
    setImage(
      photo
    );
    updateImage(photo);
  };

  return (
    <IonContent fullscreen>
      <IonTitle>
        <h1>
          <strong>{props.avion?.marque.nom}</strong>
        </h1>
      </IonTitle>
      <IonTitle>
        <h1>Numero immatricule: {props.avion?.nImmatriculation}</h1>
      </IonTitle>
      <IonCardContent>
        {props.avion?.image && (
          <img
            alt=""
            src={`data:image/${props.avion.format};base64,${props.avion.image}`}
          />
        )}
        <IonButton onClick={uploadImage}> Changer image</IonButton>
      </IonCardContent>
      <IonTitle>
        <h3>Liste des trajets </h3>
      </IonTitle>
      <IonList>
        <IonItem>
          <IonGrid>
            {props.avion?.trajets.map((trajet: Trajet) => (
              <IonRow>
                <IonCol>{trajet.dateTrajet}</IonCol>
                <IonCol>{trajet.debutKm} Km</IonCol>
                <IonCol>{trajet.finKm} Km</IonCol>
              </IonRow>
            ))}
          </IonGrid>
        </IonItem>
      </IonList>
    </IonContent>
  );
}
