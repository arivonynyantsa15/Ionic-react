import { Assurance } from '../data/interfaceResponse';
import {
  IonContent,
  IonList,
} from '@ionic/react';
import ListeAvionItems from './ListeAvionItems';

export default function ListeAssurances(props: any) {

    return (
        <IonContent fullscreen>
    
            {/* used to refresh pull */}
            {/* <IonRefresher slot="fixed" onIonRefresh={refresh}>
            <IonRefresherContent></IonRefresherContent>
            </IonRefresher> */}
    
    
            <IonList>
                {props.assurances.map((a:Assurance) => <ListeAvionItems key={a.id} avion={a.avion} />)}
            </IonList>
        </IonContent>
    );
}