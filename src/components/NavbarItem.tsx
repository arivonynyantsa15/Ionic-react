import { IonItem, IonLabel } from "@ionic/react";

export default function NavbarItem(props: { url: string; label: string}){

    return (
        <IonItem href={`${props.url}`} detail={true}>
            <IonLabel className="ion-text-wrap">
                <h3>{props.label}</h3>
            </IonLabel>
        </IonItem>

    );

}