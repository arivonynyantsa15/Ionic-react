import { Component } from "react";
import { Avion } from "../data/interfaceResponse";
import ListeAvionItems from "./ListeAvionItems";
import { IonList } from "@ionic/react";

export default class ListeAvion extends Component<{avions: Avion[]}> {

    render() {
        return (
            <IonList>
                {this.props.avions.map((avion: Avion)=> <ListeAvionItems avion={avion} />)}
            </IonList>
        )
    }
}