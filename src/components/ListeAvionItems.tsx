import { Avion } from "../data/interfaceResponse";
import {
    IonLabel,
    IonItem
} from '@ionic/react';

import React from 'react';

export default function ListeAvionItems(props: {avion: Avion}) {
    return (
        <IonItem routerLink={`/details/${props.avion.id}`}>
            <IonLabel>
                <h1>{props.avion.marque.nom}</h1>
                <h2>{props.avion.nImmatriculation}</h2>
            </IonLabel>
        </IonItem>
    )
}